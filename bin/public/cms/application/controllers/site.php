<?php

class Site extends CI_Controller{

     function __construct(){
        parent::__construct();

        if( !$this->session->userdata('admin_data') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' ){

            redirect( site_url('site/login') );
        }

        $this->load->library('grocery_CRUD');
    }

    public function index(){

        // test commit
        $this->load->view("cms/opener");
        $this->load->view("cms/closure");
    }

    public function login(){

        if( $this->session->userdata('admin_data') ){
            redirect( site_url() );
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

    public function food_list(){

        $this->grocery_crud->set_table('dish');

        $this->grocery_crud->display_as('name', 'Nombre')
        ->display_as('image', 'Imágen')
        ->display_as('description', 'Descripción');

        $this->grocery_crud->required_fields('name','image');

        $this->grocery_crud->set_field_upload('image','uploads/food');
        $this->grocery_crud->callback_before_upload(array($this,'imageValidator'));

        $this->grocery_crud->columns('name','image','description');

        $this->groceryNormalize();

        $output = $this->grocery_crud->render();

        $output->in_food_list = true;
        $output->in_food = true;

        $this->load->view("cms/opener", $output);
        $this->load->view('cms/base_render');
        $this->load->view("cms/closure");

    }

    private function groceryNormalize(){
        $this->grocery_crud->unset_fields('created_at', 'updated_at', 'remember_token','password');
    }

     // helper function for numeric dropdown grocery crud
    public function imageValidator( $files_to_upload, $field_info ){


        $allowed_extensions = array('jpg', 'jpeg', 'png');

        foreach( $files_to_upload as $file ){

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) ){

                return "Solo son permitidos archivos de tipo jpg, png";
            }
        }

        return true;

    }

    public function food_schedule(){

        $this->grocery_crud->set_table('menu');
        $this->grocery_crud->set_relation('fk_dish_id','dish','name');

        $this->grocery_crud->columns('the_date','fk_dish_id');

        $this->grocery_crud->display_as('the_date', 'Fecha')
        ->display_as('fk_dish_id', 'Plato');

        $this->grocery_crud->required_fields('the_date','fk_dish_id');

        $this->grocery_crud->order_by('the_date','DESC');

        $this->groceryNormalize();
        $output = $this->grocery_crud->render();

        $output->in_food = true;
        $output->in_food_schedule = true;

        $this->load->view("cms/opener", $output);
        $this->load->view('cms/base_render');
        $this->load->view("cms/closure");
    }

    private function ordersParams( $today = true ){
        $this->grocery_crud->set_table('vw_orders');
        $this->grocery_crud->set_primary_key('order_id');

        $this->grocery_crud->order_by('order_created_at','DESC');

        if( $today === true )
        $this->grocery_crud->where('date( order_created_at ) = ', 'date( now() )', false);

        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->columns('user_name','user_email','user_phone_home','user_phone_office','user_cellphone','user_credit','order_quantity','order_paymode','order_observations', 'order_created_at', 'order_delivered');
        $this->grocery_crud->display_as('order_paymode', 'Modo de pago')
        ->display_as('order_quantity', 'Cantidad de almuerzos')
        ->display_as('order_observations', 'Comentarios')
        ->display_as('order_delivered', 'Orden entregada')
        ->display_as('user_name', 'Nombre')
        ->display_as('user_email', 'Email')
        ->display_as('user_phone_home', 'Teléfono de casa')
        ->display_as('user_phone_office', 'Teléfono de oficina')
        ->display_as('user_cellphone', 'Celular')
        ->display_as('user_credit', 'Crédito')
        ->display_as('order_created_at', 'Fecha de pedido');

        // custom action mark as delivered
        $this->grocery_crud->add_action('Marcar como entregada', 'assets/grocery_crud/themes/flexigrid/css/images/success.png', 'site/mark_as_delivered');

        // custom action demark as delivered
        $this->grocery_crud->add_action('Marcar como no entregada', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'site/mark_as_nondelivered');

        return $this->grocery_crud->render();
    }

    public function orders(){

        $output = $this->ordersParams();

        $output->in_today_orders = true;

        $this->load->view("cms/opener", $output);
        $this->load->view('cms/order_render');
        $this->load->view("cms/closure");
    }

    public function mark_as_delivered( $order_id ){

        $this->main->updateOrder( $order_id, array('delivered' => 'yes') );
    }

    public function mark_as_nondelivered( $order_id ){

        $this->main->updateOrder( $order_id, array('delivered' => 'no') );
    }

    public function users(){
        $this->grocery_crud->set_table('user');
        $this->grocery_crud->columns('name','email','cellphone','phone_home','phone_office','credit');

        $this->grocery_crud->display_as('name', 'Nombre')
        ->display_as('email', 'Email')
        ->display_as('phone_home', 'Teléfono de casa')
        ->display_as('phone_office', 'Teléfono de oficina')
        ->display_as('cellphone', 'Celular')
        ->display_as('credit', 'Crédito');

        $this->grocery_crud->required_fields('name','email','cellphone','credit');
        $this->grocery_crud->unset_fields('password');


        $this->groceryNormalize();
        $output = $this->grocery_crud->render();

        $output->in_users = true;


        $this->load->view("cms/opener", $output);
        $this->load->view('cms/base_render');
        $this->load->view("cms/closure");
    }

    public function orders_historic(){

        $output = $this->ordersParams();

        $output->in_orders_historic = true;

        $this->load->view("cms/opener", $output);
        $this->load->view('cms/base_render');
        $this->load->view("cms/closure");
    }
}