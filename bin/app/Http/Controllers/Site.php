<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\UserAddress;
use Auth;
use App\models\Orders;
use App\models\Menu;
use App\User;

class Site extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('test/home');
    }

    public function dashboard(){

        $data = array(
            'addresses' => UserAddress::where('fk_user_id', '=', Auth::user()->id)->get()
        );

        return view('test.dashboard', $data);
    }

    public function handleLunchRequest(Request $request){

        if( $request->input('pay_mode') === NULL  ){
            return redirect()->back()->with('status', 'Debes seleccionar un modo de pago');
        }

        $Order = new Orders;

        $Order->fk_user_id = Auth::user()->id;
        // TODO change this value for the real current day menu
        $Order->fk_menu_id = Menu::first()->id;
        $Order->observations = $request->input('comments');

        // debit from credit
        if( $request->input('pay_mode') == 1 ){

            $lunch_qty = $request->input('op1_quantity');

            // credit security check
            if( $lunch_qty >  Auth::user()->credit ){
                return redirect()->back()->with('status', 'Hay una inconsistencia entre tu credito y la cantidad pedida');
            }

            $Order->quantity = $lunch_qty;
            $Order->paymode = 'Almuerzo debitado del crédito';
            $Order->save();

            // debits the order
            $user = User::find( Auth::user()->id );
            $user->credit = $user->credit - $lunch_qty;
            $user->save();

        }

        // cash pay
        if( $request->input('pay_mode') == 2 ){

            $lunch_qty = $request->input('op2_quantity');

            $Order->quantity = $lunch_qty;
            $Order->paymode = 'Almuerzo pago contra entrega';
            $Order->save();

        }


        return redirect( route('lunch-requested') );
    }

    public function lunchRequested(){

        return view('test.lunch_requested');
    }

}
