<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Route;
use Illuminate\Http\Request;
use App\models\UserAddress;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user',
            'password' => 'required|confirmed|min:4',
            'address1' => 'required|max:255',
            'address2' => 'required|max:255',
            'cellphone' => 'required|size:10'

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {

        $v = $this->validator( $request->all() );

        if( $v->fails() ){

            return redirect()->back()->withErrors($v->errors())->withInput();
        }

        // Save user
        $User = new User;

        $User->name = $request->input('name');
        $User->email = $request->input('email');
        $User->password = bcrypt($request->input('password'));
        $User->phone_home = $request->input('homephone');
        $User->phone_office = $request->input('phoneoffice');
        $User->cellphone = $request->input('cellphone');

        // TODO: Remove this line of code, just testing purposes
        $User->credit = 7;

        $User->save();

        // save addresses
        for( $i = 1; $i <= 2; $i++ ){
            $address = $request->input('address' . $i );

            if( !empty( $address ) ){
                $UserAddress = new UserAddress;
                $UserAddress->value = $address;
                $UserAddress->fk_user_id = $User->id;
                $UserAddress->save();
            }
        }

        Auth::login($User);

        return redirect( route('dashboard') );



    }

    public function index(){

        return view('test.register');
    }

    public function logIn( Request $request ){

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])){
            return redirect()->intended(route('dashboard'));
        }

        return redirect( route('home') )->with('status','Nombre de usuario o contraseña incorrecto');
    }

    public function getLogout(){

        Auth::logout();
        return redirect( route('home') );
    }
}
