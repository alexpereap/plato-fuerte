<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',[
    'as'   => 'home',
    'uses' => 'Site@index',
    'middleware' => 'guest'
]);

Route::get('register',[
    'as'   => 'register',
    'uses' => 'Auth\AuthController@index'
]);

Route::post('register',[
    'as'   => 'handle-register',
    'uses' => 'Auth\AuthController@create'
]);

Route::post('login',[
    'as'   => 'login',
    'uses' => 'Auth\AuthController@logIn'
]);

Route::group([ 'prefix' => 'user', 'middleware' => 'auth' ], function(){

    Route::get('dashboard',[
        'as'   => 'dashboard',
        'uses' => 'Site@dashboard'
    ]);

    Route::get('logout',[
        'as'   => 'logout',
        'uses' => 'Auth\AuthController@getLogout'
    ]);

    Route::post('handle-lunch-request', [
        'as' => 'handle-lunch-request',
        'uses' => 'Site@handleLunchRequest'
    ]);

    Route::get('lunch-requested',[
        'as'   => 'lunch-requested',
        'uses' => 'Site@lunchRequested'
    ]);

});