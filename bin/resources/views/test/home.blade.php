@extends('layouts.test.platofuerteapp')
@section('content')

<div class="jumbotron">
  <h1>-- info plato dia -- </h1>
  <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
</div>

<div class="row marketing">

{{ session('status') }}

  <form class="form-horizontal" action="{{ route('login') }}"  method="POST" >
  {!! Form::token() !!}
    <fieldset>

    <!-- Form Name -->
    <legend>Login</legend>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">Email</label>
      <div class="col-md-4">
      <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required="">

      </div>
    </div>

    <!-- Password input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="password">Contraseña</label>
      <div class="col-md-4">
        <input id="password" name="password" type="password" placeholder="" class="form-control input-md" required="">

      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="">&nbsp;</label>
      <div class="col-md-4">
        <button id="" name="" class="btn btn-primary">Iniciar Sesión</button>
      </div>
    </div>

    </fieldset>
</form>


</div>
@endsection