@extends('layouts.test.platofuerteapp')
@section('content')

<div class="jumbotron">
  <h1>-- info plato dia -- </h1>
  <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
</div>

<div class="row marketing">

<form class="form-horizontal" action="{{ route('handle-lunch-request') }}" method="POST" >
{!! Form::token() !!}
<fieldset>

<!-- Form Name -->
<legend>Pedir almuerzo</legend>

{{ session('status') }}

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Modo de pago</label>
  <div class="col-md-8">

  @if( Auth::user()->credit > 0 )
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="pay_mode" id="radios-0" value="1" >
      Pedir <select name="op1_quantity" > @for( $i = 1; $i <= Auth::user()->credit ; $i++ ) <option value="{{ $i }}">{{ $i }}</option>  @endfor</select> almuerzo/s de mi credito de almuerzos
    </label>
  </div>
  @endif
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="pay_mode" id="radios-1" value="2">
      Pedir <select name="op2_quantity" > @for( $i = 1; $i <= 9 ; $i++ ) <option value="{{ $i }}">{{ $i }}</option>  @endfor</select> almuerzo/s y pagar contra entrega
    </label>
  </div>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Dirección de envío</label>
  <div class="col-md-8">
    <select id="address" name="address" class="form-control">
      @foreach( $addresses as $a)
      <option value="{{ $a->id }}">{{ $a->value }}</option>
      @endforeach
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="comments">Comentarios</label>
  <div class="col-md-8">
    <textarea class="form-control" rows="4" id="comments" name="comments"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-primary">Pedir</button>
  </div>
</div>

</fieldset>
</form>


</div>
@endsection