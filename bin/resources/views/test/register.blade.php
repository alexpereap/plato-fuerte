@extends('layouts.test.platofuerteapp')
@section('content')
<div class="row marketing">

@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif

<form class="form-horizontal" method="POST" action="{{ route('handle-register') }}" >
{!! Form::token() !!}
<fieldset>

<!-- Form Name -->
<legend>Registro</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Nombre completo</label>
  <div class="col-md-4">
  <input id="name" name="name" value="{{ Input::old('name') }}" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>
  <div class="col-md-4">
  <input id="email" name="email" value="{{ Input::old('email') }}"  type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password">Contraseña</label>
  <div class="col-md-4">
    <input id="password" name="password" type="password" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password">Repetir Contraseña</label>
  <div class="col-md-4">
    <input id="password" name="password_confirmation" type="password" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address1">Dirección Oficina</label>
  <div class="col-md-4">
  <input id="address1" name="address1" value="{{ Input::old('address1') }}" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address2">Dirección Casa</label>
  <div class="col-md-4">
  <input id="address2" required name="address2" value="{{ Input::old('address2') }}" type="text" placeholder="" class="form-control input-md">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cellphone">Celular</label>
  <div class="col-md-4">
  <input id="cellphone" name="cellphone" value="{{ Input::old('cellphone') }}" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="homephone">Teléfono Casa</label>
  <div class="col-md-4">
  <input id="homephone" name="homephone" value="{{ Input::old('homephone') }}" type="text" placeholder="" class="form-control input-md">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phoneoffice">Teléfono Oficina</label>
  <div class="col-md-4">
  <input id="phoneoffice" name="phoneoffice" value="{{ Input::old('phoneoffice') }}" type="text" placeholder="" class="form-control input-md">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-primary">Registrarme</button>
  </div>
</div>

</fieldset>
</form>



</div>
@endsection