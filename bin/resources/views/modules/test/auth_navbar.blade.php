<nav>
  <ul class="nav nav-pills pull-right">
    <li role="presentation" @if( Route::getCurrentRoute()->getName() == 'dashboard' ) class="active" @endif ><a href="{{ route('dashboard') }}">Pedir Almuerzo</a></li>
    <li role="presentation" @if( Route::getCurrentRoute()->getName() == 'register' ) class="active" @endif ><a href="{{ route('register') }}">Mi Perfil</a></li>
    <li role="presentation" ><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
  </ul>
</nav>