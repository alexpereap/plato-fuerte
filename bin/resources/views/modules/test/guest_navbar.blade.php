<nav>
  <ul class="nav nav-pills pull-right">
    <li role="presentation" @if( Route::getCurrentRoute()->getName() == 'home' ) class="active" @endif ><a href="{{ route('home') }}">Home</a></li>
    <li role="presentation" @if( Route::getCurrentRoute()->getName() == 'register' ) class="active" @endif ><a href="{{ route('register') }}">Registro</a></li>
  </ul>
</nav>